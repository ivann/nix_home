{ config, pkgs, ... }:

{
  appearance = {
    font = {
      variable-width = {
        name = "Fira Sans";
        package = pkgs.fira;
      };

      monospace = {
        name = "Fira Code Nerd Font";
        package = pkgs.nerdfonts.override {
          fonts = [ "FiraCode" ];
        };
      };
    };

    colors = rec {
      base00 = "#292D3E";
      base01 = "#444267";
      base02 = "#32374D";
      base03 = "#676E95";
      base04 = "#8796B0";
      base05 = "#959DCB";
      base06 = "#959DCB";
      base07 = "#FEFEFE";
      base08 = "#F07178";
      base09 = "#F78C6C";
      base0A = "#FFCB6B";
      base0B = "#C3E88D";
      base0C = "#89DDFF";
      base0D = "#82AAFF";
      base0E = "#C792EA";
      base0F = "#FF5370";
      background = base00;
      background-light = "#474e6c";
      foreground = base07;
      foreground-dark = "#aaaaaa";
      red = base08;
      green = base0B;
      yellow = base0A;
      blue = base0D;
      magenta = base0E;
      cyan = base0C;
      orange = base09;
      brown = base0F;
      grey = base03;
      white = base07;
    };
  };
}

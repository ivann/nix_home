{ config, pkgs, ... }:

{
  imports =
    [
      ./modules/appearance
      ./appearance.nix
      ./secrets
    ];

  # Let Home Manager install and manage itself.
  programs.home-manager.enable = true;

  home.keyboard = {
    layout = "fr";
    variant = "bepo";
  };
}

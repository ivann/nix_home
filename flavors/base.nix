{ ... }:

{
  imports =
    [
      ../.
      ../roles/zsh
      ../roles/vim
    ];
}

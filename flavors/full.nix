{ config, pkgs, lib, ... }:

with lib;

let
  attrKeys = set:
    mapAttrsToList (key: _value: key) set;

  dirContent = path:
    map (p: path + "/${p}") (attrKeys (builtins.readDir path));
in
{
  imports = [ ../. ] ++ (dirContent ../roles);

  fonts.fontconfig.enable = true;

  xdg.mimeApps.enable = true;

  home.packages = with pkgs; [
    # shell
    bat
    bc
    bind
    exa
    fd
    file
    gotop
    htop
    libnotify
    lsof
    ncdu
    pciutils
    ripgrep
    sshfs
    usbutils

    # dev
    bundix
    dbeaver
    docker-compose
    hexyl
    python
    ruby

    # media
    calibre
    digikam
    exiftool
    gimp
    imagemagick
    krita
    libreoffice
    mediainfo
    mpv
    pulsemixer
    youtube-dl

    # browser
    firefox
    torbrowser

    # appearance
    lxappearance-gtk3

    # fonts
    corefonts
    symbola

    # organization
    # khal

    # security
    metasploit
    nmap

    # game
    lutris
    steam
    steam-run

    # misc
    (aspellWithDicts (d: [ d.en d.fr ]))
    jmtpfs
    maim
    mktorrent
    mumble
    ncftp
    neofetch
    nixops
    (texlive.combine {
      inherit (texlive) scheme-basic collection-luatex collection-langfrench latexmk;
    })
    # weechat
    xidlehook

    # fonts
    lmodern
    material-design-icons
    nerdfonts
  ];
}

{ config, lib, pkgs, ... }:

with lib;

let
  cfg = config.appearance;

  fontType = types.submodule {
    options = {
      package = mkOption {
        type = types.nullOr types.package;
        default = null;
        example = literalExample "pkgs.dejavu_fonts";
        description = ''
          Package providing the font. This package will be installed
          to your profile. If <literal>null</literal> then the font
          is assumed to already be available in your profile.
        '';
      };

      name = mkOption {
        type = types.str;
        example = "DejaVu Sans 8";
        description = ''
          The family name and size of the font within the package.
        '';
      };
    };
  };
in
{
  options = {
    appearance = {
      font = {
        variable-width = mkOption {
          type = types.nullOr fontType;
          default = null;
          description = ''
            The default, non monospaced, font to use.
          '';
        };

        monospace = mkOption {
          type = types.nullOr fontType;
          default = null;
          description = ''
            The default monospaced font to use.
          '';
        };
      };

      colors = mkOption {
        type = types.attrsOf types.str;
        default = {};
        example = {};
        description = ''
          Colors.
        '';
      };
    };
  };

  config =
    let
      optionalPackage = opt:
        optional (opt != null && opt.package != null) opt.package;
    in
    {
      home.packages =
        optionalPackage cfg.font.variable-width ++
        optionalPackage cfg.font.monospace;
    };
}

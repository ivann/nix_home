{ config, pkgs, lib, ... }:

{
  services = {
    dunst = {
      enable = true;
      iconTheme = {
        name = "Paper";
        package = pkgs.paper-icon-theme;
      };
      settings = with config.appearance.colors; {
        global = {
          geometry = "400x10-30+20";
          font = "${config.appearance.font.monospace.name} 10";
          padding = 8;
          horizontal_padding = 8;
          frame_width = 3;
          frame_color = blue;
          sort = "yes";
          idle_threshold = 120;
          markup = "full";
          format = "<b>%s</b>\\n%b";
          corner_radius = "4";
          timeout = 10;
        };
        urgency_low = {
          frame_color = grey;
          background = background;
          foreground = foreground;
        };
        urgency_normal = {
          frame_color = blue;
          background = background;
          foreground = foreground;
        };
        urgency_critical = {
          frame_color = red;
          background = background;
          foreground = foreground;
        };
        shortcuts = {
          close = "mod4+b";
          close_all = "mod4+shift+b";
        };
        bgnotify_success = {
          summary = "bgnotify_success";
          frame_color = green;
          background = background;
          foreground = foreground;
          format = "%b";
        };
        bgnotify_fail = {
          summary = "bgnotify_fail";
          frame_color = red;
          background = background;
          foreground = foreground;
          format = "%b";
        };
      };
    };
  };
}

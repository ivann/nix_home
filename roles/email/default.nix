{ config, pkgs, ... }:

{
  imports = [
   ../password
    ./alot
    ./astroid
  ];

  home.packages = with pkgs; [
    protonmail-bridge
    khard
    urlscan
    w3m
  ];

  accounts.email.maildirBasePath = "mail";

  programs = {
    afew = {
      enable = true;
      extraConfig = ''
        [SpamFilter.1]
        spam_tag = junk
        [KillThreadsFilter]
        [HeaderMatchingFilter.1]
        header = List-Id
        pattern = YES
        tags = +lists
        [FolderNameFilter.1]
        maildir_separator = /
        folder_lowercases = true
        [Filter.1]
        message = remove new tag
        tags = -new

        [MailMover]
        # folders = Archives Drafts Inbox Junk Sent
        folders = Inbox Junk
        rename = True

        Inbox = 'tag:junk':Junk 'NOT tag:inbox':Archives
        Junk = 'NOT tag:junk AND tag:inbox':Inbox 'NOT tag:junk':Archives

        # Archives = 'tag:deleted':Trash
        # Drafts = 'tag:deleted':Trash
        # Inbox = 'tag:junk':Junk 'NOT tag:inbox':Archive 'tag:deleted':Trash
        # Junk = 'NOT tag:junk AND tag:inbox':Inbox 'NOT tag:junk':Archive 'tag:deleted':Trash
        # Sent = 'tag:deleted':Trash
      '';
    };

    mbsync.enable = true;

    msmtp.enable = true;

    notmuch = {
      enable = true;
      new.tags = [ "new" ];
    };
  };

  home.file.syncmail = {
    target = ".local/bin/syncmail";
    text = ''
      #!/bin/sh

      ${pkgs.isync}/bin/mbsync -a
      ${pkgs.notmuch}/bin/notmuch new
      ${pkgs.afew}/bin/afew --tag --new
    '';
    executable = true;
  };
}

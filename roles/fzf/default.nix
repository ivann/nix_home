{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    bat
    fd
    file
  ];

  programs.fzf = {
    enable = true;
    changeDirWidgetCommand = "fd --type directory --follow --color=always";
    changeDirWidgetOptions = [ "--preview 'exa --tree --level 3 --color always --icons {}'" ];
    defaultCommand = "fd --type file --follow --color=always";
    defaultOptions = [
      "--height=80%"
      "--ansi"
      "--reverse"
      "--history-size=1000000000"
      "--bind 'ctrl-t:down'"
      "--bind 'ctrl-s:up'"
      "--bind 'ctrl-p:toggle-preview'"
      "--bind 'ctrl-f:page-down'"
      "--bind 'ctrl-b:page-up'"
      "--bind 'ctrl-d:half-page-down'"
      "--bind 'ctrl-u:half-page-up'"
      "--bind 'ctrl-e:jump-accept'"
    ];
    fileWidgetCommand = "fd --type file --follow --color=always";
    fileWidgetOptions = [
      ''
        --preview
          'mime=\$(file --mime {});
          (
            ([[ \$mime =~ directory ]] && exa --tree --level 3 --color always --icons {}) ||
            ([[ \$mime =~ symlink ]] && readlink {}) ||
            ([[ \$mime =~ binary ]] && echo \"\") ||
            bat --plain --color always {}
          ) 2> /dev/null'
      ''
    ];
  };
}

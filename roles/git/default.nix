{ config, pkgs, ... }:

{
  programs = {
    git = {
      enable = true;
      signing = {
        signByDefault = true;
      };
      aliases = {
        a = "add";
        s = "status";
        ci = "commit";
        co = "checkout";
        br = "branch";
        d = "diff";
        dc = "diff --cached";
        dt = "difftool";
        l = "log --oneline --graph --decorate";
      };
      lfs.enable = true;
      extraConfig = {
        merge = {
          tool = "nvim";
          conflictstyle = "diff3";
          keepBackup = false;
        };
        mergetool = {
          prompt = false;
        };
        "mergetool \"nvim\"" = {
          cmd = "nvim -f -c \\\"Gdiff\\\" \\\"$MERGED\\\"";
        };
      };
    };
  };
}

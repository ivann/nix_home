{ config, pkgs, ... }:

{
  gtk = {
    enable = true;
    font = config.appearance.font.variable-width;
    iconTheme = {
      name = "Qogir-dark";
      package = pkgs.qogir-icon-theme;
    };
    theme = {
      name = "Qogir-dark";
      package = pkgs.qogir-theme;
    };
  };
}

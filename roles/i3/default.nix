{ config, pkgs, lib, ... }:

let
  ws1 = "1 爵";
  ws2 = "2 ";
  ws3 = "3 ";
  ws4 = "4 ";
  ws5 = "5 ﬐";
  ws6 = "6 ";
  ws7 = "7";
  ws8 = "8";
  ws9 = "9";
  ws10= "10";
  modifier = "Mod4";
in
{
  home.packages = with pkgs; [
    wmfocus
    xdotool
  ];

  services = {
    picom = {
      enable = true;
      shadow = true;
    };
  };

  xsession = {
    enable = true;
    initExtra = ''
      ${pkgs.feh}/bin/feh --bg-fill ${config.home.homeDirectory}/.background-image
      ${pkgs.xidlehook}/bin/xidlehook \
        --not-when-fullscreen \
        --not-when-audio \
        --timer 300 "${pkgs.xorg.xset}/bin/xset dpms force standby" "" \
        --timer 301 "/run/wrappers/bin/slock" "" &
    '';
    numlock.enable = true;
    windowManager = {
      i3 = {
        enable = true;
        package = pkgs.i3-gaps;
        config = {
          assigns = {
            "${ws1}" = [{ class = "qutebrowser"; }];
            "${ws3}" = [{ class = "vim"; }];
            "${ws4}" = [{ class = "email"; } { class = "alot"; }];
            "${ws5}" = [{ class = "WeeChat"; }];
            "${ws7}" = [{ class = "Firefox"; }];
          };
          bars = [];
          colors = with config.appearance.colors; {
            focused = {
              border = yellow;
              background = background;
              text = foreground;
              indicator = blue;
              childBorder = cyan;
            };
          };
          floating = {
            modifier = modifier;
            criteria = [
              { window_role = "pop-up"; }
              { window_role = "task_dialog"; }
              { window_type = "splash"; }
              { title = "Preferences$"; }
              { title = "Steam - Update News"; }
              { class = "mpv"; }
              { class = "SpeedCrunch"; }
              { class = "Qalculate-gtk"; }
            ];
          };
          focus = {
            followMouse = false;
            mouseWarping = false;
          };
          fonts = [ "${config.appearance.font.variable-width.name} 10" ];
          gaps = {
            inner = 10;
            outer = -2;
            smartBorders = "on";
            smartGaps = true;
          };
          keybindings = {
            # start a terminal
            "${modifier}+Return" = "exec kitty";

            # kill focused window
            "${modifier}+Shift+x" = "kill";

            # start rofi (a program launcher)
            "${modifier}+o" = "exec rofi -show drun";
            "${modifier}+w" = "exec rofi -show window";
            "${modifier}+k" = "exec ${config.xdg.dataFile.rofiMenuScripts.target}/maimmenu.sh";

            # change focus
            "${modifier}+c" = "focus left";
            "${modifier}+t" = "focus down";
            "${modifier}+s" = "focus up";
            "${modifier}+r" = "focus right";

            # move focused window
            "${modifier}+Shift+c" = "move left";
            "${modifier}+Shift+t" = "move down";
            "${modifier}+Shift+s" = "move up";
            "${modifier}+Shift+r" = "move right";

            # split in horizontal orientation
            "${modifier}+v" = "split h";

            # split in vertical orientation
            "${modifier}+h" = "split v";

            # enter fullscreen mode for the focused container
            "${modifier}+f" = "fullscreen toggle";

            # use easyfocus
            "${modifier}+e" = ''exec wmfocus --fill --font "${config.appearance.font.variable-width.name}:72" --chars "etisuran"'';

            # change container layout (stacked, tabbed, toggle split)
            "${modifier}+eacute" = "layout tabbed";
            "${modifier}+p" = "layout toggle split";

            # toggle tiling / floating
            "${modifier}+Shift+space" = "floating toggle";

            # change focus between tiling / floating windows
            "${modifier}+space" = "focus mode_toggle";

            # focus the parent container
            "${modifier}+a" = "focus parent";

            # focus the child container
            #"${modifier}+d" = "focus child";

            # switch to workspace
            "${modifier}+quotedbl"       = "workspace ${ws1}";
            "${modifier}+guillemotleft"  = "workspace ${ws2}";
            "${modifier}+guillemotright" = "workspace ${ws3}";
            "${modifier}+parenleft"      = "workspace ${ws4}";
            "${modifier}+parenright"     = "workspace ${ws5}";
            "${modifier}+at"             = "workspace ${ws6}";
            "${modifier}+plus"           = "workspace ${ws7}";
            "${modifier}+minus"          = "workspace ${ws8}";
            "${modifier}+slash"          = "workspace ${ws9}";
            "${modifier}+asterisk"       = "workspace ${ws10}";

            # move focused container to workspace
            "${modifier}+Shift+quotedbl"       = "move container to workspace ${ws1}";
            "${modifier}+Shift+guillemotleft"  = "move container to workspace ${ws2}";
            "${modifier}+Shift+guillemotright" = "move container to workspace ${ws3}";
            "${modifier}+Shift+4"              = "move container to workspace ${ws4}";
            "${modifier}+Shift+5"              = "move container to workspace ${ws5}";
            "${modifier}+Shift+at"             = "move container to workspace ${ws6}";
            "${modifier}+Shift+plus"           = "move container to workspace ${ws7}";
            "${modifier}+Shift+minus"          = "move container to workspace ${ws8}";
            "${modifier}+Shift+slash"          = "move container to workspace ${ws9}";
            "${modifier}+Shift+asterisk"       = "move container to workspace ${ws10}";

            # focus outputs
            "${modifier}+d" = "focus output left";
            "${modifier}+l" = "focus output right";

            # move container to output
            "${modifier}+Shift+d" = "move workspace to output left";
            "${modifier}+Shift+l" = "move workspace to output right";

            # reload the configuration file
            "${modifier}+Shift+z" = "reload";
            # restart i3 inplace (preserves your layout/session, can be used to upgrade i3)
            "${modifier}+Shift+w" = "restart";
            # exit i3 (logs you out of your X session)
            "${modifier}+Shift+q" = ''exec "i3-nagbar -t warning -m 'You pressed the exit shortcut. Do you really want to exit i3? This will end your X session.' -B 'Yes, exit i3' 'i3-msg exit'"'';

            "${modifier}+m" = ''mode "resize"'';

            # audio
            "XF86AudioLowerVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ -5%";
            "XF86AudioRaiseVolume" = "exec pactl set-sink-volume @DEFAULT_SINK@ +5%";
            "XF86AudioMute" = "exec pactl set-sink-mute @DEFAULT_SINK@ toggle";

            # scratchpad
            "${modifier}+dollar" = ''exec --no-startup-id ~/${config.home.file.i3-scratchpad.target} --class=scratchpad-term ""'';
            "${modifier}+z" = ''exec --no-startup-id ~/${config.home.file.i3-scratchpad.target} --class=scratchpad-pulsemixer pulsemixer'';
          };
          modes = {
            resize = {
              "c" = "resize shrink width  10 px or 10 ppt";
              "t" = "resize grow   height 10 px or 10 ppt";
              "s" = "resize shrink height 10 px or 10 ppt";
              "r" = "resize grow   width  10 px or 10 ppt";

              "Return" = ''mode "default"'';
              "Escape" = ''mode "default"'';
              "${modifier}+m" = ''mode "default"'';
            };
          };
          modifier = "Mod4";
          startup = [
            { command = "qutebrowser"; }
            { command = "systemctl --user restart polybar"; always = true; notification = false; }
          ];
          window = {
            hideEdgeBorders = "smart";
          };
          workspaceAutoBackAndForth = true;
        };
        extraConfig = ''
          # lock the session
          bindsym --release ${modifier}+shift+j exec slock

          # scratchpad
          for_window [instance="^scratchpad-"] move to scratchpad
        '';
      };
    };
  };

  home.file.i3-scratchpad = {
    target = ".local/bin/i3-scratchpad";
    source = ./i3-scratchpad;
  };

  xdg.configFile.pulsemixerConfig = {
    target = "pulsemixer.cfg";
    source = ./pulsemixer.cfg;
  };
}

{ config, pkgs, ... }:

{
  programs.kitty = {
    enable = true;
    font = config.appearance.font.monospace;
    settings = {
      scrollback_lines = 10000;
      enable_audio_bell = false;
      update_check_interval = 0;
    };
    keybindings = {
      "ctrl+shift+s" = "scroll_line_up";
      "ctrl+shift+t" = "scroll_line_down";
      "ctrl+shift+p" = "paste_from_selection";
    };
    extraConfig = ''
      symbol_map U+2800-U+28FF Symbola
      include themes/palenight.conf
    '';
  };

  home = {
    sessionVariables = {
      TERMINAL = "kitty";
    };
  };

  xdg.configFile = {
    kittyBase16 = {
      source = (pkgs.fetchFromGitHub {
        owner = "kdrag0n";
        repo = "base16-kitty";
        rev = "742d0326db469cae2b77ede3e10bedc323a41547";
        sha256 = "19ffbvjmpla0pqqasmyfjw7hz7jzr6mhdmy8pn0babz0ff6m65fm";
      }) + /colors;
      target = "kitty/themes/base16";
    };

    kittyPalenight = {
      source = (pkgs.fetchFromGitHub {
        owner = "citizen428";
        repo = "kitty-palenight";
        rev = "6d075440890699c0f82ff781d0256e1cb9bf1635";
        sha256 = "0fsd9sj44x2phv1gk79g99ww69ygmmnrgwzy5yfdg9r8lima28av";
      }) + /palenight.conf;
      target = "kitty/themes/palenight.conf";
    };

    kittyTender = {
      source = ./kitty-tender.conf;
      target = "kitty/themes/tender.conf";
    };

    kittyGruvbox = {
      source = (pkgs.fetchFromGitHub {
        owner = "gruvbox-community";
        repo = "gruvbox-contrib";
        rev = "5be530c3bcde7f226324ec7974bd1a40a61a59d8";
        sha256 = "12cnl1agmn189ijli9sv84f0n00m2zsnvv7vgayl0ngas2g4mh07";
      }) + /kitty;
      target = "kitty/themes/gruvbox";
    };
  };
}

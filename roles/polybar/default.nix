{ config, pkgs, lib, ... }:

{
  services = {
    polybar = {
      enable = true;
      config = with config.appearance.colors; {
        "bar/base" = {
          monitor = ''''${env:MONITOR}'';
          bottom = true;
          width = "100%";
          height = 32;
          fixed-center = false;
          background = background;
          foreground = foreground-dark;

          line-size = 3;
          border-top-size = 4;

          padding-right = 1;

          module-margin-left = 1;
          module-margin-right = 2;

          font-0 = "FuraCode Nerd Font:size=10";

          modules-left = "i3";

          tray-position = "right";
          tray-padding = 2;

          cursor-click = "pointer";
          cursor-scroll = "ns-resize";
        };
        "bar/secondary" = {
          "inherit" = "bar/base";
          tray-position = "none";
          modules-right = "filesystem pulseaudio memory cpu eth date";
        };
        "bar/desktop" = {
        "inherit" = "bar/base";
        modules-right = "filesystem memory cpu temperature pulseaudio date";
      };
      "bar/laptop" = {
      "inherit" = "bar/base";
      modules-right = "filesystem xbacklight memory cpu temperature wlan eth battery pulseaudio date";
      };
      "module/filesystem" = {
        type = "internal/fs";
        interval = 5;
        format-mounted-underline = cyan;
        format-mounted = "<label-mounted> <ramp-capacity>";
        format-unmounted = "<label-unmounted>";

        mount-0 = "/";

        label-mounted = "%mountpoint%%{F-}";
        label-unmounted = "%mountpoint% not mounted";

        ramp-capacity-0-foreground = red;
        ramp-capacity-1-foreground = orange;
        ramp-capacity-2-foreground = yellow;
        ramp-capacity-0 = "▁";
        ramp-capacity-1 = "▂";
        ramp-capacity-2 = "▃";
        ramp-capacity-3 = "▄";
        ramp-capacity-4 = "▅";
        ramp-capacity-5 = "▆";
        ramp-capacity-6 = "▇";
        ramp-capacity-7 = "█";
      };
      "module/i3" = {
      type = "internal/i3";
      format = "<label-state> <label-mode>";
      index-sort = true;
      wrapping-scroll = false;

      # Only show workspaces on the same output as the bar;
      pin-workspaces = true;

      label-mode-padding = 2;
      label-mode-foreground = grey;
      label-mode-background = yellow;

      # focused = "Active workspace on focused monitor";
      label-focused = "%name%";
      label-focused-background = background-light;
      label-focused-foreground = foreground;
      label-focused-underline = yellow;
      label-focused-padding = 2;

      # unfocused = "Inactive workspace on any monitor";
      label-unfocused = "%name%";
      label-unfocused-padding = 2;

      # visible = "Active workspace on unfocused monitor";
      label-visible = "%name%";
      label-visible-underline = yellow;
      label-visible-padding = 2;

      # urgent = "Workspace with urgency hint set";
      label-urgent = "%name%";
      label-urgent-background = red;
      label-urgent-foreground = base02;
      label-urgent-padding = 2;
    };
    "module/xbacklight" = {
      type = "internal/xbacklight";

      format = "<label> <ramp>";
      label = "";
      format-underline = white;

      ramp-capacity-0 = "▁";
      ramp-capacity-1 = "▂";
      ramp-capacity-2 = "▃";
      ramp-capacity-3 = "▄";
      ramp-capacity-4 = "▅";
      ramp-capacity-5 = "▆";
      ramp-capacity-6 = "▇";
      ramp-capacity-7 = "█";
    };
    "module/cpu" = {
      type = "internal/cpu";
      interval = 2;
      format-prefix = " ";
      format-underline = red;
      format = "<ramp-load>";

      ramp-load-7-foreground = red;
      ramp-load-6-foreground = orange;
      ramp-load-5-foreground = yellow;
      ramp-load-0 = "▁";
      ramp-load-1 = "▂";
      ramp-load-2 = "▃";
      ramp-load-3 = "▄";
      ramp-load-4 = "▅";
      ramp-load-5 = "▆";
      ramp-load-6 = "▇";
      ramp-load-7 = "█";
    };
    "module/memory" = {
      type = "internal/memory";
      interval = 2;
      format-prefix = " ";
      format-underline = yellow;
      format = "<ramp-used>";

      ramp-used-7-foreground = red;
      ramp-used-6-foreground = orange;
      ramp-used-5-foreground = yellow;
      ramp-used-0 = "▁";
      ramp-used-1 = "▂";
      ramp-used-2 = "▃";
      ramp-used-3 = "▄";
      ramp-used-4 = "▅";
      ramp-used-5 = "▆";
      ramp-used-6 = "▇";
      ramp-used-7 = "█";
    };
    "module/wlan" = {
      type = "internal/network";
      interface = ''''${env:WIRELESS_INTERFACE}'';
      interval = 3;
      format-underline = grey;

      format-connected = "<ramp-signal> <label-connected>";
      format-connected-underline = green;
      label-connected = "%essid%";

      format-disconnected = "";

      ramp-signal-0-foreground = red;
      ramp-signal-1-foreground = orange;
      ramp-signal-2-foreground = yellow;
      ramp-signal-0 = "▁";
      ramp-signal-1 = "▂";
      ramp-signal-2 = "▃";
      ramp-signal-3 = "▄";
      ramp-signal-4 = "▅";
      ramp-signal-5 = "▆";
      ramp-signal-6 = "▇";
      ramp-signal-7 = "█";
    };
    "module/eth" = {
      type = "internal/network";
      interface = ''''${env:WIRED_INTERFACE}'';
      interval = 3;
      format-underline = grey;

      format-connected-prefix = " ";
      format-connected-underline = green;
      label-connected = "%local_ip%";
    };
    "module/date" = {
      type = "internal/date";
      interval = 1;

      date = "%a %d %b";
      time = "%H:%M:%S";

      format-underline = blue;

      label = " %date%  %time%";
    };
    "module/pulseaudio" = {
      type = "internal/pulseaudio";

      format-volume-underline = magenta;
      format-volume = "<label-volume> <ramp-volume>";
      label-volume = "墳";

      format-muted-underline = magenta;
      format-muted = "<label-muted>  ";
      label-muted = "婢";

      ramp-volume-0 = "▁";
      ramp-volume-1 = "▂";
      ramp-volume-2 = "▃";
      ramp-volume-3 = "▄";
      ramp-volume-4 = "▅";
      ramp-volume-5 = "▆";
      ramp-volume-6 = "▇";
      ramp-volume-7 = "█";
    };
    "module/battery" = {
      type = "internal/battery";
      battery = "BAT0";
      adapter = "AC0";
      full-at = 98;

      format-charging = " <ramp-capacity> ";
      format-charging-underline = green;

      format-discharging = " %{F${yellow}}<ramp-capacity> ";
      format-discharging-underline = ''''${self.format-charging-underline}'';

      label-full = "  ";
      format-full-underline = ''''${self.format-charging-underline}'';

      ramp-capacity-0-foreground = red;
      ramp-capacity-1-foreground = orange;
      ramp-capacity-2-foreground = yellow;
      ramp-capacity-0 = "";
      ramp-capacity-1 = "";
      ramp-capacity-2 = "";
      ramp-capacity-3 = "";
      ramp-capacity-4 = "";
      ramp-capacity-5 = "";
      ramp-capacity-6 = "";
      ramp-capacity-7 = "";
      ramp-capacity-8 = "";
      ramp-capacity-9 = "";
      ramp-capacity-10 = "";
    };
    "module/temperature" = {
      type = "internal/temperature";
      thermal-zone = 0;
      warn-temperature = 70;
      units = false;
      format-underline = "#f50a4d";
      format-warn-underline = ''''${self.format-underline}'';
      label = "%temperature-c%%{F#555}糖";
      label-warn = "%temperature-c%%{F#555}糖";
      label-warn-foreground = ''''${colors.secondary}'';
    };
    settings = {
      screenchange-reload = true;
    };
  };
  package = pkgs.polybar.override {
    i3GapsSupport = true;
    pulseSupport = true;
  };
  script = ''
    xrandr=${pkgs.xorg.xrandr}/bin/xrandr
    awk=${pkgs.gawk}/bin/awk
    hostname=${pkgs.nettools}/bin/hostname
    ip=${pkgs.iproute}/bin/ip
    grep=${pkgs.gnugrep}/bin/grep
    head=${pkgs.coreutils}/bin/head

    xrandr_output=$($xrandr)
    connected=$(echo "$xrandr_output" | $awk '/ connected/{print $1}')
    primary=$(echo "$xrandr_output" | $awk '/ primary/{print $1}')

        host_name=$($hostname)

        network_interfaces=$($ip route | $grep '^default' | $awk '{print $5}')
        wired_network_interface=$(echo "$network_interfaces" | $grep '^enp' | $head -n1)
        wireless_network_interface=$(echo "$network_interfaces" | $grep '^wlp' | $head -n1)

        for c in $connected
        do
          BAR="secondary"
          [ "$c" = "$primary" ] && BAR="$host_name"

          MONITOR=$c \
          WIRED_INTERFACE=$wired_network_interface \
          WIRELESS_INTERFACE=$wireless_network_interface \
          polybar "$BAR" &
        done
  '';
};
  };
}

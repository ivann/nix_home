{ config, pkgs, ... }:

{
  programs.qutebrowser = {
    enable = true;
    searchEngines = {
      DEFAULT = "https://duckduckgo.com/?q={}";
      w = "https://en.wikipedia.org/wiki/Special:Search?search={}&go=Go&ns0=1";
      aw = "https://wiki.archlinux.org/?search={}";
      nw = "https://nixos.wiki/index.php?search={}";
      g = "https://www.google.com/search?hl=en&q={}";
    };
    settings = {
      auto_save.session = true;
      session.lazy_restore = true;
      tabs = {
        show = "switching";
        show_switching_delay = 1000;
        position = "left";
        background = true;
      };
      content = {
        default_encoding = "utf-8";
        headers.user_agent = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 KHTML, like Gecko) Chrome/69.0.3497.100 Safari/537.36";
        webrtc_ip_handling_policy = "default-public-interface-only";
        canvas_reading = false;
        javascript.can_open_tabs_automatically = true;
      };
      downloads ={
        position = "bottom";
        remove_finished = 0;
      };
      editor.command = ["kitty" "--class" "floating" "-e" "nvim" "{}"];
      hints.chars = "auiectsrnbpovdljmhfgqkxy";
      spellcheck.languages = ["en-GB" "fr-FR"];
    };
    keyBindings = {
      normal = {
        c = "scroll left";
        r = "scroll right";
        t = "scroll down";
        s = "scroll up";
        C = "back";
        R = "forward";
        T = "tab-next";
        S = "tab-prev";
        gt = "tab-move +";
        gs = "tab-move -";
        gn = "set-cmd-text -s :buffer";
        "èc" = "back --tab";
        "èr" = "forward --tab";
        "È" = "config-cycle tabs.show always switching";
        hd = "download-clear";
        ho = "tab-only";
        J = "tab-focus";
        ",b" = "open qute://bookmarks#bookmarks";
        ",h" = "open qute://history";
        ",q" = "open qute://bookmarks";
        ",s" = "open qute://settings";
        f = "fullscreen";
        e = "hint";
        E = "hint all tab";
        we = "hint all window";
        l = "reload";
        L = "reload --force";
        F = "yank selection --sel;; later 10 open --tab --related {primary}";
        ",l" = ''config-cycle spellcheck.languages ["en-GB"] ["fr-FR"] ["en-GB", "fr-FR"]'';
        ",a" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret"'';
        ",u" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret" --username-only'';
        ",p" = ''spawn --userscript qute-pass --username-pattern "login: (.+)" --username-target "secret" --password-only'';
      };
      caret = {
        C = "scroll left";
        R = "scroll right";
        T = "scroll down";
        S = "scroll up";
        c = "move-to-prev-char";
        r = "move-to-next-char";
        t = "move-to-next-line";
        s = "move-to-prev-line";
        F = "yank selection --sel;; later 10 open --tab --related {primary}";
      };
    };
    extraConfig = ''
      config.source('themes/minimal/base16-onedark.config.py')
      c.colors.webpage.bg = 'white'
    '';
  };

  xdg = {
    configFile = {
      qutebrowserTheme = {
        source = (pkgs.fetchFromGitHub {
          owner = "theova";
          repo = "base16-qutebrowser";
          rev = "c9af94e0b0ffc8895a2e4ab24e261b0841212aa6";
          sha256 = "012h6vh8y1vfjflkyf7nzaci8l99d1ya7a3wv8kva63yxr80qllw";
        }) + /themes;
        target = "qutebrowser/themes";
      };
    };

    mimeApps.defaultApplications = {
      "application/x-extension-htm" = "org.qutebrowser.qutebrowser.desktop";
      "application/x-extension-html" = "org.qutebrowser.qutebrowser.desktop";
      "text/html" = "org.qutebrowser.qutebrowser.desktop";
      "x-scheme-handler/http" = "org.qutebrowser.qutebrowser.desktop";
      "x-scheme-handler/https" = "org.qutebrowser.qutebrowser.desktop";
    };
  };
}

{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    atool
    bat
    catdoc
    djvulibre
    exiftool
    ffmpegthumbnailer
    file
    fontforge
    imagemagick
    jq
    libarchive
    mediainfo
    odt2txt
    pandoc
    poppler_utils
    ranger
    xlsx2csv
  ];

  programs.zsh.shellAliases = {
    r = "ranger --choosedir=$HOME/.cache/ranger/lastdir; cd $(cat $HOME/.cache/ranger/lastdir)";
  };

  xdg = {
    configFile = {
      rangerConfig = {
        source = ./rc.conf;
        target = "ranger/rc.conf";
      };

      rangerScope = {
        source = ./scope.sh;
        target = "ranger/scope.sh";
      };

      rangerDeviconsPlugin = {
        target = "ranger/plugins/ranger_devicons";
        source = (pkgs.fetchFromGitHub {
          owner = "alexanderjeurissen";
          repo = "ranger_devicons";
          rev = "1fa1d0f29047979b9ffd541eb330756ac4b348ab";
          sha256 = "0lvcfykhxsjcz2ipxlldasclz459arya4q8b9786kbqp2y1k6z5k";
        });
      };
    };

    dataFile = {
      rangerDesktop = {
        source = ./ranger.desktop;
        target = "applications/ranger.desktop";
      };
   };
  };
}

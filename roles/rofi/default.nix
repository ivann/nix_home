{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    maim
    xdotool
    xclip
    imagemagick
    libnotify
  ];

  programs.rofi = {
    enable = true;
    font = "${config.appearance.font.variable-width.name} 12";
    lines = 10;
    location = "top";
    terminal = "kitty";
    extraConfig = ''
      rofi.theme: rofi-menu/appsmenu
      rofi.modi: window,run,ssh,drun,combi
      rofi.monitor: 1
      rofi.fixed-num-lines: false
      rofi.show-icons: true
      rofi.scroll-method: 1
      rofi.drun-match-fields: name
      rofi.hide-scrollbar: true
      rofi.columns: 2
      rofi.modi: drun
      rofi.display-drun: apps
      rofi.kb-row-down: Ctrl+t
      rofi.kb-row-up: Ctrl+s
      rofi.kb-row-up: ISO_Left_Tab
    '';
  };

  xdg.dataFile = {
    rofiBase16 = {
      source = (pkgs.fetchFromGitHub {
        owner = "0xdec";
        repo = "base16-rofi";
        rev = "afbc4b22d8f415dc89f36ee509ac35fb161c6df4";
        sha256 = "1f9gkfc4icdgdj0fkkgg1fw3n6imlr1sbi42qm9hbkjxy5fmzay2";
      }) + /themes;
      target = "rofi/themes/base16";
    };

    rofiMenuColors = {
      text = with config.appearance.colors; ''
        * {
        accent:           ${yellow};
        background:       ${background};
        background-light: ${background-light};
        background-focus: ${grey};
        foreground:       ${foreground};
        foreground-list:  ${foreground-dark};
        on:               ${green};
        off:              ${red};
        }
      '';
      target = "rofi/themes/rofi-menu-colors.rasi";
    };

    rofiMenuThemes = {
      source = ./rofi-menu/themes;
      target = "rofi/themes/rofi-menu";
    };

    rofiMenuScripts = {
      source = ./rofi-menu/scripts;
      target = "rofi/scripts/rofi-menu";
    };
  };
}

#!/usr/bin/env bash

rofi_command="rofi -theme rofi-menu/maimmenu.rasi"

### Options ###
screen=""
area=""
window=""
color=""
dir="$HOME/screens"
# Variable passed to rofi
options="$screen\n$area\n$window\n$color"

screenshot_name() {
  echo "$dir/$(date +"%F_%H-%M-%S").png"
}

take_screenshot() {
  sn=$(screenshot_name)
  maim --delay=1 --hidecursor $maim_options $sn
  xclip -selection clipboard -target image/png $sn
}

mkdir -p $dir

chosen="$(echo -e "$options" | $rofi_command -dmenu -selected-row 1)"
case $chosen in
    $screen)
        take_screenshot
        ;;
    $area)
        maim_options="--select" take_screenshot
        ;;
    $window)
        maim_options="--window=$(xdotool getactivewindow)" take_screenshot
        ;;
    $color)
        color="#$(maim -st 0 | convert - -alpha off -resize 1x1\! -format '%[hex:p{0,0}]' info:-)"
        echo -n "$color" | xclip
        notify-send "color" "$color"
        ;;
esac


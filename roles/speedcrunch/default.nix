{ config, pkgs, ... }:

{
  home.packages = with pkgs; [
    speedcrunch
  ];

  xdg.dataFile = {
    speedcrunchTheme = {
      target = "SpeedCrunch/color-schemes/theme.json";
      text = with config.appearance.colors; builtins.toJSON {
        cursor = base0E;
        number = base05;
        parens = base09;
        result = base0B;
        comment = base03; 
        matched = base0A;
        function = base0D;
        operator = base0C;
        variable = base08;
        scrollbar = base01;
        separator = base03;
        background = background;
        editorbackground = background-light;
      };
    };
  };
}

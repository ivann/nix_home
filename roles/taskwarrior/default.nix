{ config, pkgs, ... }:

{
  programs = {
    taskwarrior = {
      enable = true;
    };

    zsh.shellAliases = {
      t = "task";
    };
  };
}

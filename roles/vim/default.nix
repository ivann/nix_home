{ config, pkgs, ... }:

{
  home = {
    packages = with pkgs; [
      ctags
      global
      nixpkgs-fmt
      nodejs
      ripgrep
      solargraph
      vim-vint
    ] ++ (with pkgs.vimPlugins; [
      coc-css
      coc-git
      coc-highlight
      coc-html
      coc-json
      coc-pairs
      coc-python
      coc-snippets
      coc-solargraph
      coc-tsserver
      coc-vimtex
      coc-yaml
    ]);

    sessionVariables = {
      EDITOR = "nvim";
      MANPAGER = "nvim -c 'set ft=man' -";
    };
  };

  programs = {
    neovim = {
      enable = true;
      vimAlias = true;
      plugins = with pkgs.vimPlugins; [
        # General
        editorconfig-vim
        pear-tree
        vim-startify
        vim-dadbod
        vim-endwise
        vim-hardtime
        undotree
        vim-polyglot
        vim-highlightedyank
        vim-cool
        vim-easymotion

        # Navigation
        defx-nvim
        defx-icons
        defx-git
        denite
        denite-extra
        denite-git
        neomru-vim
        vista-vim

        # Colors
        base16-vim
        palenight-vim
        tender-vim
        gruvbox-community
        rainbow

        # Snippets
        vim-snippets

        # Completion
        coc-nvim
        gen_tags-vim

        # Text manipulation
        increment-activator
        vim-abolish
        vim-commentary
        vim-surround
        vim-easy-align
        vim-swap

        # Linter
        ale

        # Git
        vimagit
        vim-fugitive

        # UI
        lightline-vim
        lightline-ale
        indentLine
        vim-devicons

        # Organization
        vimwiki
      ];
      extraConfig = builtins.readFile ./vimrc;
    };
  };

  xdg.configFile = {
    cocSettings = {
      target = "nvim/coc-settings.json";
      text = builtins.toJSON {
        diagnostic = {
          displayByAle = true;
        };
        languageserver = {
          elixirLS = {
            command = "elixir-ls";
            filetypes = [ "elixir" "eelixir" ];
          };
        };
        javascript.preferences = {
          noSemicolons = true;
          quoteStyle = "single";
        };
        python = {
          pythonPath = "./env/python";
          formatting.provider = "black";
          formatting.blackPath = "./env/black";
          linting = {
            pylintEnabled = false;
            pylamaEnabled = true;
            pylamaPath = "./env/pylama";
          };
          sortImports.path = "./env/isort";
        };
        solargraph = {
          checkGemVersion = false;
          diagnostics = true;
          formatting = true;
        };
      };
    };

    lightlineColorschemePalenight = {
      source = ./lightline/colorscheme/palenight.vim;
      target = "nvim/autoload/lightline/colorscheme/palenight.vim";
    };
  };
}

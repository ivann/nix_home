{ config, pkgs, ... }:

let
  ohMyZshRepo = pkgs.fetchFromGitHub {
    owner = "ohmyzsh";
    repo = "ohmyzsh";
    rev = "610b2529d2213a70e3d1153a9baf046c22f298b9";
    sha256 = "1dwv67s1f63a3rjca3ak4sfrab87hjmqm3kfcrsm0iy8mjrr4jkv";
  };
in
{
  home.packages = with pkgs; [
    direnv
    exa
    libnotify
  ];

  programs = {
    command-not-found.enable = true;

    pazi.enable = true;

    zsh = {
      enable = true;
      enableAutosuggestions = true;
      enableCompletion = true;
      defaultKeymap = "viins";
      dotDir = ".config/zsh";
      history = {
        expireDuplicatesFirst = true;
        path = ".local/share/zsh/history";
        size = 1000000000;
      };
      envExtra = ''
        export DIRENV_LOG_FORMAT=
        export PATH=$HOME/.local/bin:$PATH
      '';
      initExtraBeforeCompInit = with config.appearance.colors; ''
        bgnotify_threshold=0

        function bgnotify_formatted { ## args: (exit_status, command, elapsed_seconds)
          elapsed="$(( $3 % 60 ))s"
          (( $3 >= 60 )) && elapsed="$((( $3 % 3600) / 60 ))m $elapsed"
          (( $3 >= 3600 )) && elapsed="$(( $3 / 3600 ))h $elapsed"
          if [ $1 -eq 0 ]; then
            bgnotify "bgnotify_success" "<span foreground=\"${green}\">✔</span> $elapsed \n$2"
          else
            bgnotify "bgnotify_fail" "<span foreground=\"${red}\">✘</span> $elapsed \n$2"
          fi
        }
      '';
      initExtra = ''
        unalias run-help
        autoload -Uz run-help
        autoload -Uz run-help-git
        autoload -Uz run-help-ip
        autoload -Uz run-help-openssl
        autoload -Uz run-help-sudo
        autoload -Uz edit-command-line
        autoload -Uz history-search-end

        # Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
        # Initialization code that may require console input (password prompts, [y/n]
        # confirmations, etc.) must go above this block, everything else may go below.
        if [[ -r "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh" ]]; then
          source "''${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-''${(%):-%n}.zsh"
        fi

        # To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
        [[ ! -f ~/.config/zsh/p10k.zsh ]] || source ~/.config/zsh/p10k.zsh

        ZSH_CACHE_DIR=$HOME/.cache/zsh
        [ -d "$ZSH_CACHE_DIR" ] || mkdir -p "$ZSH_CACHE_DIR"

        autoload -Uz colors
        colors


        ### Options

        ## Changing Directories

        # cd = pushd
        setopt AUTO_PUSHD
      
        # remove duplicate
        setopt PUSHD_IGNORE_DUPS

        # make pushd silent
        setopt PUSHD_SILENT
      
        # blank pushd goes to home
        setopt PUSHD_TO_HOME


        ## Completion

        setopt AUTO_MENU

        setopt COMPLETE_IN_WORD


        ## Expansion and Globbing

        # allow #, ~ and ^ in filename patterns
        setopt EXTENDED_GLOB

        # don't throw an error when no match are found
        setopt NO_NOMATCH
      
        # sort numeric filenames numerically
        setopt NUMERIC_GLOB_SORT

        ## History

        # save each command's beginning timestamp and duration
        setopt EXTENDED_HISTORY

        # remove the older duplicates
        setopt HIST_IGNORE_ALL_DUPS

        # do not save command starting by a space to the history
        setopt HIST_IGNORE_SPACE

        # Remove superfluous blanks before recording entry
        setopt HIST_REDUCE_BLANKS

        # share history between sessions
        setopt SHARE_HISTORY

        ## Input/Output

        # spell check commands
        setopt CORRECT

        # disable flow control (^s and ^q)
        setopt NO_FLOW_CONTROL

        ## Job Control

        # report background and suspended jobs before exiting a shell
        setopt CHECK_JOBS

        ## Zle

        # disable beeps
        setopt NO_BEEP


        ### Completion

        zstyle ':completion:*' accept-exact '*(N)'
        # completion caching, rehash to clear
        zstyle ':completion:*' use-cache on
        zstyle ':completion:*' cache-path "$ZSH_CACHE_DIR/completion"

        # menu if nb items > 2
        zstyle ':completion:*' menu select=2

        # hilight ambiguous character
        zstyle ':completion:*' show-ambiguity

        # colors
        zstyle -e ':completion:*:default' list-colors 'reply=("''${PREFIX:+=(#bi)($PREFIX:t)(?)*==34=34}:''${(s.:.)LS_COLORS}")';

        # completers
        zstyle ':completion:*' completer _complete _match _approximate

        # Increase the number of errors based on the length of the typed word. But make
        # sure to cap (at 7) the max-errors to avoid hanging.
        zstyle ':completion:*:approximate:*' max-errors 1 numeric
        zstyle -e ':completion:*:approximate:*' max-errors 'reply=($((($#PREFIX+$#SUFFIX)/3>7?7:($#PREFIX+$#SUFFIX)/3))numeric)'

        # ignore what's already on the line
        zstyle ':completion:*:(rm|kill|killall|diff|cp|mv):*' ignore-line yes
 
        zstyle ':completion:*' ignore-parents parents pwd
 
        zstyle ':completion:*' list-separator '┆ '
 
        zstyle ':completion:*' verbose yes
        zstyle ':completion:*:descriptions' format '%F{yellow}%B── %d ──%b%f'
        zstyle ':completion:*:messages' format '%F{red}%d%f'
        zstyle ':completion:*' group-name '''
        zstyle ':completion:*:manuals' separate-sections true
 
        zstyle ':completion:*:processes' command 'ps -u $LOGNAME -o pid,user,command -w'
        zstyle ':completion:*:*:kill:*:processes' list-colors "=(#b) #([0-9]#)*=29=34"
 
        # Don't complete uninteresting users...
        zstyle ':completion:*:*:*:users' ignored-patterns \
          adm amanda apache avahi beaglidx bin cacti canna clamav daemon \
          dbus distcache dovecot fax ftp games gdm gkrellmd gopher \
          hacluster haldaemon halt hsqldb ident junkbust ldap lp mail \
          mailman mailnull mldonkey mysql nagios \
          named netdump news nfsnobody nobody nscd ntp nut nx openvpn \
          operator pcap postfix postgres privoxy pulse pvm quagga radvd \
          rpc rpcuser rpm shutdown squid sshd sync uucp vcsa xfs '_*'

        # ... unless we really want to.
        zstyle '*' single-ignored show

        zstyle :compinstall filename "$HOME/.config/zsh/.zshrc"


        ### Binding

        zle -N edit-command-line
        zle -N history-beginning-search-backward-end history-search-end
        zle -N history-beginning-search-forward-end history-search-end

        ## bind special keys
        # https://wiki.archlinux.org/index.php/Zsh#Key_bindings

        # create a zkbd compatible hash;
        # to add other keys to this hash, see: man 5 terminfo
        typeset -A key

        key[Home]=''${terminfo[khome]}
        key[End]=''${terminfo[kend]}
        key[Insert]=''${terminfo[kich1]}
        key[Delete]=''${terminfo[kdch1]}
        key[Up]=''${terminfo[kcuu1]}
        key[Down]=''${terminfo[kcud1]}
        key[Left]=''${terminfo[kcub1]}
        key[Right]=''${terminfo[kcuf1]}
        key[PageUp]=''${terminfo[kpp]}
        key[PageDown]=''${terminfo[knp]}
        key[STab]=''${terminfo[kcbt]}

        # setup key accordingly
        [[ -n "''${key[Home]}"     ]]  && bindkey -- "''${key[Home]}"     beginning-of-line
        [[ -n "''${key[End]}"      ]]  && bindkey -- "''${key[End]}"      end-of-line
        [[ -n "''${key[Insert]}"   ]]  && bindkey -- "''${key[Insert]}"   overwrite-mode
        [[ -n "''${key[Delete]}"   ]]  && bindkey -- "''${key[Delete]}"   delete-char
        [[ -n "''${key[Up]}"       ]]  && bindkey -- "''${key[Up]}"       history-beginning-search-backward-end
        [[ -n "''${key[Down]}"     ]]  && bindkey -- "''${key[Down]}"     history-beginning-search-forward-end
        [[ -n "''${key[Left]}"     ]]  && bindkey -- "''${key[Left]}"     backward-char
        [[ -n "''${key[Right]}"    ]]  && bindkey -- "''${key[Right]}"    forward-char
        [[ -n "''${key[PageUp]}"   ]]  && bindkey -- "''${key[PageUp]}"   beginning-of-buffer-or-history
        [[ -n "''${key[PageDown]}" ]]  && bindkey -- "''${key[PageDown]}" end-of-buffer-or-history
        [[ -n "''${key[STab]}"     ]]  && bindkey -- "''${key[STab]}"     reverse-menu-complete 

        # Finally, make sure the terminal is in application mode, when zle is
        # active. Only then are the values from $terminfo valid.
        if (( ''${+terminfo[smkx]} && ''${+terminfo[rmkx]} )); then
          autoload -Uz add-zle-hook-widget
          function zle_application_mode_start {
            echoti smkx
          }
          function zle_application_mode_stop {
            echoti rmkx
          }
          add-zle-hook-widget -Uz zle-line-init zle_application_mode_start
          add-zle-hook-widget -Uz zle-line-finish zle_application_mode_stop
        fi

        bindkey ' ' magic-space

        bindkey -M viins '^p' history-beginning-search-backward-end
        bindkey -M vicmd '^p' history-beginning-search-backward-end

        bindkey -M viins '^n' history-beginning-search-forward-end
        bindkey -M vicmd '^n' history-beginning-search-forward-end

        bindkey -M viins '^w' backward-kill-word

        # edit current command line in vim with CTRL-f
        bindkey -M vicmd '^e' edit-command-line
        bindkey -M viins '^e' edit-command-line

        # open help with CTRL-h
        bindkey -M viins '^h'   run-help
        bindkey -M vicmd '^h'   run-help

        # bepo
        bindkey -M vicmd '*'     beginning-of-line
        bindkey -M vicmd 's'       history-beginning-search-backward-end
        bindkey -M vicmd 't'     history-beginning-search-forward-end
        bindkey -M vicmd 'c'     backward-char
        bindkey -M vicmd 'r'    forward-char


        ### Misc

        eval $(dircolors -b $HOME/${config.xdg.configFile.dircolors.target})

        compinit -d "$ZSH_CACHE_DIR/zcompdump"

        eval "$(direnv hook zsh)"

        nixify() {
          if [ ! -e ./.envrc ]; then
            echo "use nix" > .envrc
            direnv allow
          fi
          if [ ! -e default.nix ]; then
            cat > shell.nix <<'EOF'
        with import <nixpkgs> {};

        mkShell {
          name = "env";

          buildInputs = [
          ];
        }
        EOF
            ''${EDITOR:-vim} default.nix
          fi
        }
      '';
      localVariables = {
        KEYTIMEOUT = 1;
        ZSH_HIGHLIGHT_HIGHLIGHTERS = [ "main" "brackets" ];
      };
      plugins = [
        {
          name = "powerlevel10k";
          src = pkgs.fetchFromGitHub {
            owner = "romkatv";
            repo = "powerlevel10k";
            rev = "c3ddca85ff33f30daf209e1863459f3ca38ce054";
            sha256 = "1b608br2r1p4j414rlmn9clxq7w57vx83y7mz65s3f4d71ah0sk9";
          };
          file = "powerlevel10k.zsh-theme";
        }
        {
          name = "zsh-syntax-highlighting";
          src = pkgs.fetchFromGitHub {
            owner = "zsh-users";
            repo = "zsh-syntax-highlighting";
            rev = "34df84a7dd88e55a629d90f644e021d995e1a616";
            sha256 = "1d1xz49lip70dj0p619l812c0p2v6j64x80kgcjfglk7xyc7qjyy";
          };
        }
        {
          name = "zsh-autopair";
          src = pkgs.fetchFromGitHub {
            owner = "hlissner";
            repo = "zsh-autopair";
            rev = "34a8bca0c18fcf3ab1561caef9790abffc1d3d49";
            sha256 = "1h0vm2dgrmb8i2pvsgis3lshc5b0ad846836m62y8h3rdb3zmpy1";
          };
        }
        {
          name = "bgnotify";
          src = ohMyZshRepo + /plugins/bgnotify;
        }
        {
          name = "dirpersist";
          src = ohMyZshRepo + /plugins/dirpersist;
        }
        {
          name = "fancy-ctrl-z";
          src = ohMyZshRepo + /plugins/fancy-ctrl-z;
        }
      ];
      shellAliases = {
        hs = "history | grep";
        psg = "ps aux | grep";
        l = "exa -l --git --icons";
        lt = "l --tree";
        ls = "ls --color=auto";
        grep = "grep --color=auto";
      };
    };
  };

  xdg.configFile = {
    p10kConfig = {
      target = "zsh/p10k.zsh";
      source = ./p10k.zsh;
    };

    dircolors = {
      target = "dircolors";
      source = (pkgs.fetchFromGitHub {
        owner = "iv-nn";
        repo = "LS_COLORS";
        rev = "396233ece44bec519e26962917de190b1f50a556";
        sha256 = "1q06zbgxnmricqm8ccv7sq7658n29dgxljha0q25mpfv9wzcdfr7";
      }) + /LS_COLORS;
    };
  };
}
